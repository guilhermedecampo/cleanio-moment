Package.describe({
  summary: "Moment.js"
});

Package.on_use(function (api, where) {
  if(api.export) {
    api.export('moment');
  }
  where = where || ['client', 'server'];
  api.add_files('lib/moment/moment.js', where);
  api.add_files('export-moment.js', where);
  api.add_files('moment-range.js', where);
  api.add_files('require-shim.js', where);
  api.add_files('lib/moment-timezone/moment-timezone.js', where);
  api.add_files('lib/moment-timezone-data/moment-timezone-data.js', where);
  api.add_files('revert-require-shim.js', where);
});
